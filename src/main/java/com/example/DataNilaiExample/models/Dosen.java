package com.example.DataNilaiExample.models;
// Generated Jul 1, 2022, 8:36:17 AM by Hibernate Tools 4.3.6.Final

import java.util.HashSet;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 * Dosen generated by hbm2java
 */
@Entity
@Table(name = "dosen", schema = "public")
public class Dosen implements java.io.Serializable {

	private long dosenId;
	private User user;
	private String nama;
	private Set<DataNilai> dataNilais = new HashSet<DataNilai>(0);

	public Dosen() {
	}

	public Dosen(long dosenId, User user, String nama) {
		this.dosenId = dosenId;
		this.user = user;
		this.nama = nama;
	}

	public Dosen(long dosenId, User user, String nama, Set<DataNilai> dataNilais) {
		this.dosenId = dosenId;
		this.user = user;
		this.nama = nama;
		this.dataNilais = dataNilais;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "generator_id_dosen_seq")
	@SequenceGenerator(name="generator_id_dosen_seq", sequenceName="id_dosen_seq", schema = "public", allocationSize = 1)
	@Column(name = "dosen_id", unique = true, nullable = false)
	public long getDosenId() {
		return this.dosenId;
	}

	public void setDosenId(long dosenId) {
		this.dosenId = dosenId;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "user_id")
	public User getUser() {
		return this.user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	@Column(name = "nama")
	public String getNama() {
		return this.nama;
	}

	public void setNama(String nama) {
		this.nama = nama;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "dosen")
	public Set<DataNilai> getDataNilais() {
		return this.dataNilais;
	}

	public void setDataNilais(Set<DataNilai> dataNilais) {
		this.dataNilais = dataNilais;
	}

}
