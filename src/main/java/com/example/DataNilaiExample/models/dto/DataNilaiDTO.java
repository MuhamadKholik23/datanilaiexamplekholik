package com.example.DataNilaiExample.models.dto;


public class DataNilaiDTO {
	private long dataNilaiId;
	private DosenDTO dosen;
	private MahasiswaDTO mahasiswa;
	private MataKuliahDTO mataKuliah;
	private int nilai;
	private String keterangan;
	
	public DataNilaiDTO() {
		// TODO Auto-generated constructor stub
	}

	public DataNilaiDTO(long dataNilaiId, DosenDTO dosen, MahasiswaDTO mahasiswa, MataKuliahDTO mataKuliah, int nilai,
			String keterangan) {
		super();
		this.dataNilaiId = dataNilaiId;
		this.dosen = dosen;
		this.mahasiswa = mahasiswa;
		this.mataKuliah = mataKuliah;
		this.nilai = nilai;
		this.keterangan = keterangan;
	}

	public long getDataNilaiId() {
		return dataNilaiId;
	}

	public void setDataNilaiId(long dataNilaiId) {
		this.dataNilaiId = dataNilaiId;
	}

	public DosenDTO getDosen() {
		return dosen;
	}

	public void setDosen(DosenDTO dosen) {
		this.dosen = dosen;
	}

	public MahasiswaDTO getMahasiswa() {
		return mahasiswa;
	}

	public void setMahasiswa(MahasiswaDTO mahasiswa) {
		this.mahasiswa = mahasiswa;
	}

	public MataKuliahDTO getMataKuliah() {
		return mataKuliah;
	}

	public void setMataKuliah(MataKuliahDTO mataKuliah) {
		this.mataKuliah = mataKuliah;
	}

	public int getNilai() {
		return nilai;
	}

	public void setNilai(int nilai) {
		this.nilai = nilai;
	}

	public String getKeterangan() {
		return keterangan;
	}

	public void setKeterangan(String keterangan) {
		this.keterangan = keterangan;
	}
	
	
}
