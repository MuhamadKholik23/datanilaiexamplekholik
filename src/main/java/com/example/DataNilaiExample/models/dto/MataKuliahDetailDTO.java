package com.example.DataNilaiExample.models.dto;

public class MataKuliahDetailDTO {
	private long mataKuliahDetailId;
	private String namaMataKuliah;
	
	public MataKuliahDetailDTO() {
		// TODO Auto-generated constructor stub
	}

	public MataKuliahDetailDTO(long mataKuliahDetailId, String namaMataKuliah) {
		super();
		this.mataKuliahDetailId = mataKuliahDetailId;
		this.namaMataKuliah = namaMataKuliah;
	}

	public long getMataKuliahDetailId() {
		return mataKuliahDetailId;
	}

	public void setMataKuliahDetailId(long mataKuliahDetailId) {
		this.mataKuliahDetailId = mataKuliahDetailId;
	}

	public String getNamaMataKuliah() {
		return namaMataKuliah;
	}

	public void setNamaMataKuliah(String namaMataKuliah) {
		this.namaMataKuliah = namaMataKuliah;
	}
	
	
}
