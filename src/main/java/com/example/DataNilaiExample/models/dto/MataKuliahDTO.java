package com.example.DataNilaiExample.models.dto;

import com.example.DataNilaiExample.models.Mahasiswa;
import com.example.DataNilaiExample.models.MataKuliahDetail;

public class MataKuliahDTO {
	private long mataKuliahId;
	private Mahasiswa mahasiswa;
	private MataKuliahDetailDTO mataKuliahDetail;
	
	public MataKuliahDTO() {
		// TODO Auto-generated constructor stub
	}

	public MataKuliahDTO(long mataKuliahId, Mahasiswa mahasiswa, MataKuliahDetailDTO mataKuliahDetail) {
		super();
		this.mataKuliahId = mataKuliahId;
		this.mahasiswa = mahasiswa;
		this.mataKuliahDetail = mataKuliahDetail;
	}

	public long getMataKuliahId() {
		return mataKuliahId;
	}

	public void setMataKuliahId(long mataKuliahId) {
		this.mataKuliahId = mataKuliahId;
	}

	public Mahasiswa getMahasiswa() {
		return mahasiswa;
	}

	public void setMahasiswa(Mahasiswa mahasiswa) {
		this.mahasiswa = mahasiswa;
	}

	public MataKuliahDetailDTO getMataKuliahDetail() {
		return mataKuliahDetail;
	}

	public void setMataKuliahDetail(MataKuliahDetailDTO mataKuliahDetail) {
		this.mataKuliahDetail = mataKuliahDetail;
	}
	
	
	
}
