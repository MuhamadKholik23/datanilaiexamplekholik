package com.example.DataNilaiExample.models.dto;

import java.util.Date;


public class MahasiswaDTO {
	private long nim;
	private UserDTO user;
	private String nama;
	private Date tanggalLahir;
	private String jurusan;
	private String alamat;
	
	public MahasiswaDTO() {
		// TODO Auto-generated constructor stub
	}

	public MahasiswaDTO(long nim, UserDTO user, String nama, Date tanggalLahir, String jurusan, String alamat) {
		super();
		this.nim = nim;
		this.user = user;
		this.nama = nama;
		this.tanggalLahir = tanggalLahir;
		this.jurusan = jurusan;
		this.alamat = alamat;
	}

	public long getNim() {
		return nim;
	}

	public void setNim(long nim) {
		this.nim = nim;
	}

	public UserDTO getUser() {
		return user;
	}

	public void setUser(UserDTO user) {
		this.user = user;
	}

	public String getNama() {
		return nama;
	}

	public void setNama(String nama) {
		this.nama = nama;
	}

	public Date getTanggalLahir() {
		return tanggalLahir;
	}

	public void setTanggalLahir(Date tanggalLahir) {
		this.tanggalLahir = tanggalLahir;
	}

	public String getJurusan() {
		return jurusan;
	}

	public void setJurusan(String jurusan) {
		this.jurusan = jurusan;
	}

	public String getAlamat() {
		return alamat;
	}

	public void setAlamat(String alamat) {
		this.alamat = alamat;
	}
	
	
}
