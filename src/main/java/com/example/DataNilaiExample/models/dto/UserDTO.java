package com.example.DataNilaiExample.models.dto;

public class UserDTO {
	private long userId;
	private String email;
	private String pasword;
	private String role;
	
	public UserDTO() {
		// TODO Auto-generated constructor stub
	}

	public UserDTO(long userId, String email, String pasword, String role) {
		super();
		this.userId = userId;
		this.email = email;
		this.pasword = pasword;
		this.role = role;
	}

	public long getUserId() {
		return userId;
	}

	public void setUserId(long userId) {
		this.userId = userId;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPasword() {
		return pasword;
	}

	public void setPasword(String pasword) {
		this.pasword = pasword;
	}

	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}
	
	
}
