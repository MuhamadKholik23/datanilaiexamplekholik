package com.example.DataNilaiExample.models.dto;

import com.example.DataNilaiExample.models.User;

public class DosenDTO {
	private long dosenId;
	private User user;
	private String nama;
	
	public DosenDTO() {
		// TODO Auto-generated constructor stub
	}

	public DosenDTO(long dosenId, User user, String nama) {
		super();
		this.dosenId = dosenId;
		this.user = user;
		this.nama = nama;
	}

	public long getDosenId() {
		return dosenId;
	}

	public void setDosenId(long dosenId) {
		this.dosenId = dosenId;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public String getNama() {
		return nama;
	}

	public void setNama(String nama) {
		this.nama = nama;
	}
	
	
}
