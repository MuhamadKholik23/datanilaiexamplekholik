package com.example.DataNilaiExample.controller;

import java.util.List;
import java.util.stream.Collectors;

import javax.annotation.security.RolesAllowed;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.example.DataNilaiExample.helper.XLSHelper;
import com.example.DataNilaiExample.models.Dosen;
import com.example.DataNilaiExample.models.Mahasiswa;
import com.example.DataNilaiExample.models.User;
import com.example.DataNilaiExample.models.request.LoginRequest;
import com.example.DataNilaiExample.models.response.JwtResponse;
import com.example.DataNilaiExample.models.response.MessageResponse;
import com.example.DataNilaiExample.repositories.DosenRepository;
import com.example.DataNilaiExample.repositories.MahasiswaRepository;
import com.example.DataNilaiExample.repositories.UserRepository;
import com.example.DataNilaiExample.security.jwt.JwtUtils;
import com.example.DataNilaiExample.security.services.UserDetailsImpl;
import com.example.DataNilaiExample.service.MahasiswaService;

@RestController
@RequestMapping("/api/upload")
public class UploadFileController {
	@Autowired
	private MahasiswaService mahasiswaService;
	@Autowired
	MahasiswaRepository mahasiswaRepo ;
	@Autowired
	UserRepository userRepo ;
	@Autowired
	DosenRepository dosenRepo;
	@Autowired
	AuthenticationManager authenticationManager;
	@Autowired
	JwtUtils jwtUtils;
	
	
	@PreAuthorize("hasRole('DOSEN')")
	@PostMapping("/file")
	  public ResponseEntity<MessageResponse> uploadFile(@RequestParam("file") MultipartFile file) {
	    String message = "";

		    if (XLSHelper.hasExcelFormat(file)) {
		      try {
		    	mahasiswaService.save(file);
		        message = "Uploaded the file successfully: " + file.getOriginalFilename();
		        return ResponseEntity.status(HttpStatus.OK).body(new MessageResponse(message));
		      } catch (Exception e) {
		        message = "Could not upload the file: " + file.getOriginalFilename() + "!";
		        return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(new MessageResponse(message));
		      }
		    }
	    message = "Please upload an excel file!";
	    return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new MessageResponse(message));
	  }

	

}
