package com.example.DataNilaiExample.controller;


import java.util.ArrayList;

import java.util.HashSet;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.DataNilaiExample.models.Dosen;
import com.example.DataNilaiExample.models.ERole;
import com.example.DataNilaiExample.models.Mahasiswa;
import com.example.DataNilaiExample.models.Role;
import com.example.DataNilaiExample.models.User;
import com.example.DataNilaiExample.models.request.LoginRequest;
import com.example.DataNilaiExample.models.request.SignupRequest;
import com.example.DataNilaiExample.models.response.JwtResponse;
import com.example.DataNilaiExample.models.response.MessageResponse;
import com.example.DataNilaiExample.repositories.DosenRepository;
import com.example.DataNilaiExample.repositories.MahasiswaRepository;
import com.example.DataNilaiExample.repositories.RoleRepository;
import com.example.DataNilaiExample.repositories.UserRepository;
import com.example.DataNilaiExample.security.jwt.JwtUtils;
import com.example.DataNilaiExample.security.services.UserDetailsImpl;


@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/api/auth")
public class AuthController {
	@Autowired
	AuthenticationManager authenticationManager;

	@Autowired
	UserRepository userRepository;

	@Autowired
	RoleRepository roleRepository;

	@Autowired
	PasswordEncoder encoder;

	@Autowired
	JwtUtils jwtUtils;
	
	@Autowired
	MahasiswaRepository mahasiswaRepo ;
	
	@Autowired
	DosenRepository dosenRepo ;

	@PostMapping("/signin")
	public ResponseEntity<?> authenticateUser(@Valid @RequestBody LoginRequest loginRequest) {
		
		Authentication authentication = authenticationManager.authenticate(
				new UsernamePasswordAuthenticationToken(loginRequest.getUsername(), loginRequest.getPassword()));
		SecurityContextHolder.getContext().setAuthentication(authentication);
		String jwt = jwtUtils.generateJwtToken(authentication);
		
		UserDetailsImpl userDetails = (UserDetailsImpl) authentication.getPrincipal();		
		List<String> roles = userDetails.getAuthorities().stream()
				.map(item -> item.getAuthority())
				.collect(Collectors.toList());
		return ResponseEntity.ok(
				new JwtResponse(jwt, userDetails.getId(), userDetails.getUsername(), userDetails.getEmail(), roles));
	}

	@PostMapping("/signup")
	public ResponseEntity<?> registerUser(@Valid @RequestBody SignupRequest signUpRequest) {
		if (userRepository.existsByUsername(signUpRequest.getUsername())) {
			return ResponseEntity.badRequest().body(new MessageResponse("Error: Username is already taken!"));
		}

		if (userRepository.existsByEmail(signUpRequest.getEmail())) {
			return ResponseEntity.badRequest().body(new MessageResponse("Error: Email is already in use!"));
		}

		// Create new user's account
		User user = new User(signUpRequest.getUsername(), signUpRequest.getEmail(),
				encoder.encode(signUpRequest.getPassword()));
		Mahasiswa newMahasiswa = new Mahasiswa();
		Dosen newDosen = new Dosen();
//		Set<String> strRoles = signUpRequest.getRole();
		String strRoles = signUpRequest.getRole();
		Set<Role> roles = new HashSet<>();

//		if (strRoles == null) {
//			Role adminRole = roleRepository.findByName(ERole.ROLE_ADMIN)
//					.orElseThrow(() -> new RuntimeException("Error: Role is not found."));
//			roles.add(adminRole);
//		} else {
//			strRoles.forEach(role -> {
//				switch (role) {
//				case "mahasiswa":
//					Role mahasiswaRole = roleRepository.findByName(ERole.ROLE_MAHASISWA)
//							.orElseThrow(() -> new RuntimeException("Error: Role is not found."));
//					roles.add(mahasiswaRole);
//					
//					break;
//				case "dosen":
//					Role dosenRole = roleRepository.findByName(ERole.ROLE_DOSEN)
//							.orElseThrow(() -> new RuntimeException("Error: Role is not found."));
//					roles.add(dosenRole);
//				
//					break;
//				default:
//					Role adminRole = roleRepository.findByName(ERole.ROLE_ADMIN)
//							.orElseThrow(() -> new RuntimeException("Error: Role is not found."));
//					roles.add(adminRole);
//				}
//			});
//		}
		Role entityRole = new Role();
		if(strRoles.toLowerCase().equalsIgnoreCase("mahasiswa")) {
			entityRole = roleRepository.findByName(ERole.ROLE_MAHASISWA)
					.orElseThrow(() -> new RuntimeException("Error: Role is not found."));
		}else if(strRoles.toLowerCase().equalsIgnoreCase("dosen")) {
			entityRole = roleRepository.findByName(ERole.ROLE_DOSEN)
					.orElseThrow(() -> new RuntimeException("Error: Role is not found."));
		}
		
		

//		user.setRoles(roles);
		user.setRole(entityRole);
		userRepository.save(user);
		if(strRoles.toLowerCase().equalsIgnoreCase("mahasiswa")) {
			newMahasiswa.setUser(user);
			mahasiswaRepo.save(newMahasiswa);
		}else if(strRoles.toLowerCase().equalsIgnoreCase("dosen")) {
			newDosen.setUser(user);
			dosenRepo.save(newDosen);
		}
//		for(String roleUser : strRoles) {
//			if(roleUser.toLowerCase().equalsIgnoreCase("mahasiswa")) {
//				newMahasiswa.setUser(user);
//				mahasiswaRepo.save(newMahasiswa);
//			}else if(roleUser.toLowerCase().equalsIgnoreCase("dosen")) {
//				newDosen.setUser(user);
//				dosenRepo.save(newDosen);
//			}
//		}
		
		return ResponseEntity.ok(new MessageResponse("User registered successfully!"));
		
	}

}
