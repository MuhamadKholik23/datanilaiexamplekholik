package com.example.DataNilaiExample.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.DataNilaiExample.models.MataKuliah;

@Repository
public interface MataKuliahRepository extends JpaRepository<MataKuliah, Long>{

}
