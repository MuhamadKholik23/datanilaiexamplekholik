package com.example.DataNilaiExample.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.DataNilaiExample.models.Dosen;

@Repository
public interface DosenRepository extends JpaRepository<Dosen, Long>{

}
