package com.example.DataNilaiExample.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.DataNilaiExample.models.MataKuliahDetail;

@Repository
public interface MataKuliahDetailRepository extends JpaRepository<MataKuliahDetail, Long>{

}
