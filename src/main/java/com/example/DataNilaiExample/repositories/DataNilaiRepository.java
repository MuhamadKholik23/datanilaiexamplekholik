package com.example.DataNilaiExample.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.DataNilaiExample.models.DataNilai;

@Repository
public interface DataNilaiRepository extends JpaRepository<DataNilai, Long>{

}
