package com.example.DataNilaiExample.helper;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.web.multipart.MultipartFile;
import com.example.DataNilaiExample.models.Mahasiswa;
public class XLSHelper {
	
	private static final String TYPE = "xlsx";
//	private static String[] HEADERS = { "nim", "nama", "jurusan", "alamat", "tanggalLahir" };
	private static String SHEET = "Mahasiswa";
	
	public static boolean hasExcelFormat(MultipartFile file) {
//	    if (!TYPE.equals(file.getContentType())) {
//	      return false;
//	    }
	    return true;
	}
	
	public static List<Mahasiswa> excelMahasiswa(InputStream is){
		try {
			 Workbook workbook = new XSSFWorkbook(is);
			 Sheet sheet = workbook.getSheet(SHEET);
		     Iterator<Row> rows = sheet.iterator();
		     List<Mahasiswa> listMahasiswa = new ArrayList<>();
		     int rowNumber = 0;
		     while (rows.hasNext()) {
		         Row currentRow = rows.next();
		         // skip header
		         if (rowNumber == 0) {
		           rowNumber++;
		           continue;
		         }
		         Iterator<Cell> cellsInRow = currentRow.iterator();
		         Mahasiswa mahasiswaEntity = new Mahasiswa();
		         int cellIdx = 0;
		         while (cellsInRow.hasNext()) {
		           Cell currentCell = cellsInRow.next();
		           switch (cellIdx) {
		           case 0:
		        	 mahasiswaEntity.setNim((long)(currentCell.getNumericCellValue()));
		             break;
		           case 1:
		        	 mahasiswaEntity.setNama(currentCell.getStringCellValue());
		             break;
		           case 2:
		        	 mahasiswaEntity.setJurusan(currentCell.getStringCellValue());
		             break;
		           case 3:
		        	 mahasiswaEntity.setAlamat(currentCell.getStringCellValue());
		             break;
		           case 4:
		        	 mahasiswaEntity.setTanggalLahir(currentCell.getDateCellValue());
			         break;
		           default:
		             break;
		           }
		           cellIdx++;
		         }
		         listMahasiswa.add(mahasiswaEntity);
		       }
		       workbook.close();
		       return listMahasiswa;
		}catch(IOException ex) {
			throw new RuntimeException("fail to parse Excel file: " + ex.getMessage());
		}
	}
	
}
