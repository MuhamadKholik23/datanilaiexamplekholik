package com.example.DataNilaiExample.service;

import java.io.IOException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.example.DataNilaiExample.helper.XLSHelper;
import com.example.DataNilaiExample.models.Mahasiswa;
import com.example.DataNilaiExample.repositories.MahasiswaRepository;

@Service("MahasiswaService")
public class MahasiswaService {
	
	@Autowired
	MahasiswaRepository mahasiswaRepo ;
	
	public void save(MultipartFile file) {
	    try {
	      List<Mahasiswa> listMahasiswa = XLSHelper.excelMahasiswa(file.getInputStream());
	      mahasiswaRepo.saveAll(listMahasiswa);
	    } catch (IOException ex) {
	      throw new RuntimeException("fail to store excel data: " + ex.getMessage());
	    }
	}
	
	
	  public List<Mahasiswa> getAllTutorials() {
	    return mahasiswaRepo.findAll();
	  }
	
}
