package com.example.DataNilaiExample;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DataNilaiExampleApplication {

	public static void main(String[] args) {
		SpringApplication.run(DataNilaiExampleApplication.class, args);
	}

}
